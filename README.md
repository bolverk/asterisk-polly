# Dependencies

1. Perl Modules
    1. Paws
    2. Asterisk::AGI
    3. Digest::SHA
2. madplay
3. sox

# Additional requirements

1. Cache directory must exist and be writeable
2. AWS credentials stored in ~/.aws/credentials
