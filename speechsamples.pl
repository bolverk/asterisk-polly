#!/usr/bin/perl -w

use Paws;

use strict;

my %config = (
	region => 'us-east-1',
	engine => 'neural',
	languageCode => 'en-US',
	voiceid => 'Salli',
	sampleRate => 16000,
);

my %langMap = (
	'Ivy' => 'en-US',
	'Joanna' => 'en-US',
	'Kendra' => 'en-US',
	'Kimberly' => 'en-US',
	'Salli' => 'en-US',
	'Joey' => 'en-US',
	'Justin' => 'en-US',
	'Kevin' => 'en-US',
	'Matthew' => 'en-US',
	'Olivia' => 'en-AU',
	'Amy' => 'en-GB',
	'Emma' => 'en-GB',
	'Brian' => 'en-GB',
	'Aria' => 'en-NZ',
);

foreach my $voiceID (keys %langMap) {
	my $out = getSpeech(
		voiceid => $voiceID,
		languageCode => $langMap{$voiceID},
		text => 'This is a test of the Loyalty Forge broadcasting system. '.
		  'If this was a real message, it would be read to you by your phone system.',
	);

	print 'Voice ID: ' . $voiceID ."\n";
	print 'Content Type: ' . $out->ContentType ."\n";
	print 'Request Characters: ' . $out->RequestCharacters . "\n";

	open OUT, ">$voiceID.mp3";
	print OUT $out->AudioStream;
	close OUT;
}

exit;

sub getSpeech {
	my %speechConfig = @_;
	die "No text specified for getSpeech()\n" unless defined($speechConfig{text});

	my $polly = Paws->service('Polly', region => defined($speechConfig{region})?$speechConfig{region}:$config{region});

	my $SynthesizeSpeechOutput = $polly->SynthesizeSpeech(
		'OutputFormat' => 'mp3',
		'TextType'     => 'text',
		'LanguageCode' => defined($speechConfig{languageCode})?$speechConfig{languageCode}:$config{languageCode},
		'Engine'       => defined($speechConfig{engine})?$speechConfig{engine}:$config{engine},
		'SampleRate'   => defined($speechConfig{sampleRate})?$speechConfig{sampleRate}:$config{sampleRate},
		'Text'         => $speechConfig{text},
		'VoiceId'      => defined($speechConfig{voiceid})?$speechConfig{voiceid}:$config{voiceid},
	);
	 
	return $SynthesizeSpeechOutput;
};
